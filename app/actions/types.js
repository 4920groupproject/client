export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGGED_IN = 'LOGGED_IN';
export const LOGGED_OUT = 'LOGGED_OUT';
export const CREATED_GROUP = 'CREATED_GROUP';
