import React, { Component } from "react"
import {
  StatusBar,
  Text,
  TouchableOpacity,
  View,
  FlatList,
} from "react-native"

import Icon from 'react-native-vector-icons/Ionicons'
import ProfileComponent from '../../components/drawer/ProfileComponent'
import DrawerItem from '../../components/drawer/DrawerItemComponent'
import { styles, menuData } from './DrawerMenu.styles' 
import { api } from '../../config/api'

const REQUEST_URL = 'https://aqueous-bayou-78971.herokuapp.com';

class DrawerMenu extends Component {
  constructor(props){
    super(props)
    this.state = {
      username: '',
      email: '',
      avatar: 'http://res.cloudinary.com/devwj3eid/image/upload/v1508479797/blank_profile_picture_aajkxd.png'
    }
  }

  componentDidMount() { 
    drawerViewObj = this; // set as global var for use in helper functions
    getUserDetails(); 
  }

  render() {
    return (
      <View style={styles.container}>
        <ProfileComponent profileUrl={this.state.avatar} username={this.state.username} email={this.state.email} />
        <FlatList
          data={menuData}
          renderItem={({item}) => 
              <DrawerItem navigation={this.props.navigation} screenName={item.screenName} icon={item.icon} name={item.name} key={item.key} />
            
          }
        />
      </View>
    );
  }
}

async function getUserDetails() {
  var _email = await api.get('user')  
  var  avatarUrl = await api.get('avatar')
  console.log("Avatar Url: ", avatarUrl)
  drawerViewObj.setState({
    username: 'Navigation',
    email: _email.substr(1).slice(0, -1), 
  })
  if(avatarUrl != null) {
    drawerViewObj.setState({
      avatar: 'http://res.cloudinary.com/devwj3eid/image/upload/' + avatarUrl + 'png'
    })
  }
}

DrawerMenu.defaultProps = {};

DrawerMenu.propTypes = {};

export default DrawerMenu;
