import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'rgba(255,255,255,0.43)'
  },
  menuItem: {
    flexDirection:'row'
  },
  menuItemText: {
    fontSize:15,
    fontWeight:'300',
    margin:15,
  }
})

export const menuData = [
  {icon: "ios-home-outline", name:"Home", screenName:"Groups", key: 1},
  {icon: "ios-log-out", name:"Logout", screenName:"Start", key: 4},
]

//  {icon: "ios-search", name:"Search", screenName:"Groups", key: 1},
// {icon: "ios-chatboxes-outline", name:"Chats", screenName:"Groups", key: 3},
