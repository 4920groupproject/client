import React, { Component } from 'react';
import { StyleSheet, Dimensions, Image, } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text,
         View, DeckSwiper, Card, CardItem, Thumbnail,
       } from 'native-base';
       

import styles from './styles/AppHeader.styles';

class AppHeader extends Component {
  render() {
    return (
      <Header style={styles.header}>
        <Left>
          <Button transparent 
            onPress={() => this.props.navigation.navigate('DrawerToggle')}
          >
            <Icon name='menu' />
          </Button>
        </Left>
        <Body>
          <Title>Keebus [Logo]</Title>
        </Body>
        <Right>
          <Button transparent style={{ width:30, paddingLeft:5, paddingRight:5}}>
            <Icon name='chatbubbles' style={{fontSize: 20, color: 'white'}} />
          </Button>
        </Right>
      </Header>
     );
  }
}

export default AppHeader;
