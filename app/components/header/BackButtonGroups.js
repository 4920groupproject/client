import React, { Component } from 'react';
import { Button, Icon, Text } from 'native-base';
import { BackHandler } from 'react-native';
import { NavigationActions } from 'react-navigation'

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'Groups'})
  ]
})


class BackButtonGroups extends Component {
    
    // Disable android back button
    // modified from https://stackoverflow.com/questions/40145301/preventing-hardware-back-button-android-for-react-native, by Jickson.
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        return true;
    }
  
  render() {
    return (
      <Button transparent iconLeft light
        onPress={
          () => {
          this.props.navigation.dispatch(resetAction)
          this.props.navigation.navigate('Groups');
            if(this.props.serverPoller != undefined){
              clearInterval(this.props.serverPoller);
            }
          }
      }>
        <Icon name='arrow-back' style={{color:'#2980b9'}}/>
        <Text style={{color:'#2980b9'}}>Back</Text>
      </Button>
     );
  }
}

export default BackButtonGroups;
