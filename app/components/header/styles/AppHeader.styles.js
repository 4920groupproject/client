import { StyleSheet, Dimensions, } from 'react-native';

var SCREEN_HEIGHT = Dimensions.get('window').height;
var SCREEN_WIDTH  = Dimensions.get('window').width;

const styles = StyleSheet.create({
  header: {
    height: SCREEN_HEIGHT * .1,
  },
});

export default styles;
