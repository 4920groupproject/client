import React, { Component } from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  ListView,
  Image,
  StyleSheet
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

import { LOGGED_OUT } from "../../actions/types"
import store from '../../store'

const DrawerItem = ({ navigation, icon, name, screenName }) =>
  <TouchableOpacity
    style={styles.menuItem}
    onPress={() => {
      if (name === 'Logout') {
        store.dispatch({type: LOGGED_OUT})
      }
      navigation.navigate(`${screenName}`, { isStatusBarHidden: false })}
    }
  >
    <Icon name ={icon} size={25} color="#333" style={{margin:15}} />
    <Text style={styles.menuItemText}>{name}</Text>
  </TouchableOpacity>


const styles = StyleSheet.create({
  menuItem: {
    flexDirection:'row'
  },
  menuItemText: {
    fontSize:15,
    fontWeight:'300',
    margin:15,
  }
})

export default DrawerItem
