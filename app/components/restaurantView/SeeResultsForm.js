import React, { Component } from 'react';
import { 
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import styles from './styles/SeeResultsForm.styles';

class SeeResultsForm extends Component {
  render() {
    return (

      <View style={styles.container}>
        <TouchableOpacity style={styles.buttonContainer} 
            onPress={ () => this.props.navigation.navigate('Results') }
        >
          <Text style={styles.buttonText}>See most popular</Text>
        </TouchableOpacity>
        
        <TouchableOpacity style={styles.buttonContainer} 
            onPress={ () => this.props.navigation.navigate('Groups') }
        >
          <Text style={styles.buttonText}>See groups</Text>
        </TouchableOpacity>
      </View>
     );
  }
}

export default SeeResultsForm;
