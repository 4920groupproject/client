import {  StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    padding: 20,
    marginTop: '50%',
  },
  buttonContainer: {
    backgroundColor: '#2980b9',
    paddingVertical: 15,
    marginTop:20,
  },
  buttonText: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontWeight: '700',
  }
})

export default styles;