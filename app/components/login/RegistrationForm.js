import React, { Component } from 'react';
import { 
  TextInput,
  View,
  TouchableOpacity,
  Text,
  Alert
} from 'react-native';

import styles from './styles/RegistrationForm.styles';
import { api } from '../../config/api';


class RegistrationForm extends Component {

  constructor(props) {
    super(props)
    this.state = {
      username: '',
      email: '',
      password: '',
      confirmPassword: '',
      firstname: '',
      lastname: '',
    }
  }

  render() {
    const nav = this.props.navigation; 
    return (
      <View style={styles.container}>
        <TextInput 
          placeholder=" Firstname"
          placeholderTextColor="rgba(255,255,255, 0.7)"
          style={styles.input}
          underlineColorAndroid={'transparent'}
          onChangeText={(firstname) => this.setState({firstname})}
          value={this.state.firstname}
          />    
        <TextInput 
          placeholder=" Lastname"
          placeholderTextColor="rgba(255,255,255, 0.7)"
          style={styles.input}
          underlineColorAndroid={'transparent'}
          onChangeText={(lastname) => this.setState({lastname})}
          value={this.state.lastname}
          />    
        <TextInput 
          placeholder=" username"
          placeholderTextColor="rgba(255,255,255, 0.7)"
          style={styles.input}
          underlineColorAndroid={'transparent'}
          onChangeText={(username) => this.setState({username})}
          value={this.state.username}
          />    
        <TextInput 
          placeholder=" email"
          placeholderTextColor="rgba(255,255,255, 0.7)"
          style={styles.input}
          underlineColorAndroid={'transparent'}
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
          />
        <TextInput 
          placeholder=" password"
          placeholderTextColor="rgba(255,255,255, 0.7)"
          secureTextEntry
          style={styles.input}
          underlineColorAndroid={'transparent'}
  onChangeText={(password) => this.setState({password})}
          value={this.state.password}
          />    
        <TextInput 
          placeholder=" confirm password"
          placeholderTextColor="rgba(255,255,255, 0.7)"
          secureTextEntry
          style={styles.input}
          underlineColorAndroid={'transparent'}
  onChangeText={(confirmPassword) => this.setState({confirmPassword})}
          value={this.state.confirmPassword}
          />
        <TouchableOpacity style={styles.buttonContainer}
          onPress={ async () => {
              const avatar = await api.get("avatar") 
              console.log("AVATARR IS: ", avatar)
              const uploadStatus = await api.get("uploadStatus") 
              console.log("uploadSatus IS: ", uploadStatus)
              if(this.state.password === this.state.confirmPassword){
                api.register(this.state.username, this.state.email, this.state.password, this.state.firstname, this.state.lastname, nav)
              } else {
                Alert.alert(
                  "Registration Failed",
                  "Passwords do not match!",
                  [ {text: 'OK', onPress: () => {/* add a function on OK */}},],
                  { cancelable: false }
                )
              }
            }
          }
        >
          <Text style={styles.buttonText}>REGISTER</Text>
        </TouchableOpacity>
      </View>
     );
  }
}
export default RegistrationForm;
