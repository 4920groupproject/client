import {  StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    padding: 20   
  },
  buttonContainer: {
    backgroundColor: '#2980b9',
    paddingVertical: 15,
    marginBottom: 10
  },
  buttonText: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontWeight: '700'
  }
})

export default styles;
