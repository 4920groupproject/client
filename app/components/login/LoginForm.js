import React, { Component } from 'react';
import { 
  TextInput,
  View,
  TouchableOpacity,
  Text,
  ScrollView
} from 'react-native';

import styles from './styles/LoginForm.styles';
import { api } from '../../config/api';

class LoginForm extends Component {
  
  constructor(props) {
    super(props)
    this.state = {
      user: '',
      password: '',
    }
  }

  render() {
    const nav = this.props.navigation; 
    return (
      <ScrollView style={styles.container}>
        <TextInput 
          placeholder="username or email"
          placeholderTextColor="rgba(255,255,255, 0.7)"
          style={styles.input}
          underlineColorAndroid={'transparent'}
          onChangeText={(user) => this.setState({user})}
          value={this.state.user}
          />    
        <TextInput 
          placeholder="password"
          placeholderTextColor="rgba(255,255,255, 0.7)"
          secureTextEntry
          style={styles.input}
          underlineColorAndroid={'transparent'}
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
          />    
        <TouchableOpacity style={styles.buttonContainer}
            onPress={ () => {
                api.login(this.state.user,this.state.password,nav)
              } 
            }
        >
          <Text style={styles.buttonText}>LOGIN</Text>
        </TouchableOpacity>
      </ScrollView>
     );
  }
}

export default LoginForm;
