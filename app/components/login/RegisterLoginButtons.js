import React, { Component } from 'react';
import { 
  TextInput,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import styles from './styles/RegisterLoginButtons.styles';

function RegisterLoginButtons({ navigation }) {
    return (
      <View style={styles.container}>
        <TouchableOpacity 
          style={styles.buttonContainer} 
          onPress={() => navigation.navigate('Login')}
        >
            <Text style={styles.buttonText}>LOGIN</Text>
          </TouchableOpacity>

        <TouchableOpacity 
          style={styles.buttonContainer}
          onPress={() => navigation.navigate('Register')}
        >
          <Text style={styles.buttonText}>REGISTER</Text>
        </TouchableOpacity>
      </View>
     );
}

export default RegisterLoginButtons;
