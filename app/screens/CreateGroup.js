import React, { Component } from 'react';
import { 
  Image,
  View,
  KeyboardAvoidingView,
  ActivityIndicator,
  TouchableOpacity,
  Clipboard,
  TextInput,
  AsyncStorage
} from 'react-native'
import { Button, Icon, Text } from 'native-base';

import styles from './styles/CreateGroup.styles';
import { api } from "../config/api";
import { CREATED_GROUP } from "../actions/types"
import store from '../store'

/* The structure of this file has been copied from LoginForm.js -Irfan */

/* Not sure what these are -Irfan */
// const options = {
  // title: 'Create a group',
  // storageOptions: {
    // skipBackup: true,
    // path: 'images',
  // },
// }


const REQUEST_URL = 'https://aqueous-bayou-78971.herokuapp.com';

var createGroupObj; // set as global var for use in helper functions (see assignment below)

class CreateGroup extends Component {
  constructor(props) {
    super(props)
    this.state = {
      link: "None",
      groupName: "",
      fetchFailed: false,
    };
  }

  
  componentDidMount() { 
    createGroupObj = this; // set as global var for use in helper functions
  }
  
  render() {
      
    return(
      <KeyboardAvoidingView behaviour="padding" style={styles.container}>
      {/* There seems to be a bug with ios when using KeyboardAvoidingView */}

        <Button transparent iconLeft light
          onPress={ () => {
            this.props.navigation.navigate('Groups')
          }
        }>
          <Icon name='arrow-back' />
          <Text>Back</Text>
        </Button>

        
        <TextInput 
          placeholder="Group Name"
          placeholderTextColor="rgba(255,255,255, 0.7)"
          style={styles.input}
          underlineColorAndroid={'transparent'}
          onChangeText={(groupName) => this.setState({groupName})}
          value={this.state.groupName}
        />    

        <TouchableOpacity style={styles.buttonContainer}
            onPress={ () => {
                fetchGroupLink();
                this.setState({
                  link: "Loading",
                });
              }
            }
        >
          <Text style={styles.buttonText}>CREATE GROUP</Text>
        </TouchableOpacity>
     
        
        <Text style={styles.linkLabel}>
          Sharable Link:
        </Text>
        <Text style={styles.link}>
          {this.state.link}
        </Text>
        
        <TouchableOpacity style={styles.buttonContainer}
            onPress={ () => Clipboard.setString(this.state.link) }
        >
          <Text style={styles.buttonText}>Copy link to clipboard</Text>
        </TouchableOpacity>

      </KeyboardAvoidingView>
      
    );
  }
}


// Create a group
async function fetchGroupLink(){
  const token = await api.get('token')
  const data = { name: createGroupObj.state.groupName }
  fetch(REQUEST_URL + '/creategroup', {  
    method: 'POST',  
    headers: {  
      'Authorization': 'JWT ' + token,
      'Accept': 'application/json',
      'Content-type':  'application/json'
    }, 
    body: JSON.stringify(data),
  })
  .then((response) => {
    // If an error is sent by the server
    if(response.status < 200 || response.status >= 300){ // success codes are 2xx
      createGroupObj.setState({
        fetchFailed: true,
        link: "Could not connect to server",
      });
      return null;
    }
    return response.json();
  })
  .then((responseJson) =>{
    // If server response is successful
    if(responseJson != null){
      createGroupObj.setState({
        link: responseJson.link,
      });
      store.dispatch({type: CREATED_GROUP, groupName: responseJson.name})
    }
  })
  .catch((error) => {
    // Catch any other errors
    console.error(error);
    createGroupObj.setState({
      fetchFailed: true,
      link: "Could not connect to server",
    });
  });
}

export default CreateGroup; 
