/*
Notes:
  - NativeBase is less robust when handling images;
    if the image is not found it crashes.
  - Should switch to HTTPS later
  - References at end of file
*/

import React, { Component } from 'react';
import { Platform, StyleSheet, Dimensions, Image, FlatList } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text,
         View, Thumbnail,
       } from 'native-base';
import { api } from '../config/api'

import AppHeader from '../components/header/AppHeader';
import BackButtonGroups from '../components/header/BackButtonGroups';
import styles from './styles/Results.styles';

const SERVER_ADDR = 'https://aqueous-bayou-78971.herokuapp.com' // 'http://192.168.0.20:8081/'

var resultsViewObj; // set as global var for use in helper functions (see assignment below)

export default class Results extends Component {
  constructor(props) {
    super(props);
    this.state = {
      restaurantsList: [],
      tempRestaurantsList: [{"id": "4f89212bf76dde31f092cfc14d7506555d85b5c7", "name": "Google", "rating": 4.4, "vicinity": "48 Pirrama Road, Pyrmont", "photoAttribution": "<a href=\"https://maps.google.com/maps/contrib/100231481477080708236/photos\">Andy Trickett</a>", "photoUrl": "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=CmRaAAAAx6i_EbYyKIW-nplZms47HvhCgfA0W7hl65Iu4DSO8Yj4Jk0GjKAy2ahBNWmTrQRXwQIJXsCt1ewd0LOe4ixQJDYcvCoEOqF5T3hf7IjetEDhG0UPRDeGRoCUPNOEG4V5EhDIQiSNF53veQdMijz5uACEGhTcY-Sgc464lU3fITRtbruPQi1b9w&key=AIzaSyCgg6AEmeAGsMOwB7Z4NhxBQUel5_3rgPw", "votes": 3}, {"id": "a97f9fb468bcd26b68a23072a55af82d4b325e0d", "name": "Australian Cruise Group", "rating": 4.7, "vicinity": "32 The Promenade, King Street Wharf 5, Sydney", "photoAttribution": "<a href=\"https://maps.google.com/maps/contrib/110751364053842618118/photos\">Australian Cruise Group</a>", "photoUrl": "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=CmRaAAAAtM5oJvynxKrbzZ-TkWlk30JXL5Zfs6yJ00NdFUW5-xmgngP3ltxCxd8fHYnh4CwYcIHsHdtI_agmYPE12PFatPa86IUDyOGje8dKxVEi3egvTtnwlm9jE7YwuxN6psCiEhB4D7ZbbVko1eGlTLKP45M-GhTsLH3eSCFEZLJe39W-L8t9eqMRiw&key=AIzaSyCgg6AEmeAGsMOwB7Z4NhxBQUel5_3rgPw", "votes": 3}],
      fetchFailed: false,
      noRestaurantsFound: false,
      // userId: 3456, // IMPLEMENT THIS!!!
      // groupId: 7, // IMPLEMENT THIS!!!
    }
  }

  componentDidMount() { 
    resultsViewObj = this; // set as global var for use in helper functions
    fetchMostPopular(); 
  }
  
  render() {
    
    {/* The order of these if statements for various errors is important */}
    
    if(this.state.fetchFailed){
      return (
        <View>
          <BackButtonGroups navigation={this.props.navigation}/>
          <Text style={styles.preloadMessage}>Could not fetch from server.</Text>
        </View>
      );
    }
    
    if(this.state.noRestaurantsFound){
      return (
        <View>
          <BackButtonGroups navigation={this.props.navigation}/>
          <Text style={styles.preloadMessage}>No popular restaurants yet.</Text>
        </View>
      );
    }
    
    if (this.state.restaurantsList.length == 0){
      return (
        <View>
          <BackButtonGroups navigation={this.props.navigation}/>
          <View><Text style={styles.preloadMessage}>Loading Popular Results...</Text></View>
        </View>
      );
    }
    
    {/* If no errors: */}
    return (
      <Container>

        {/*<AppHeader navigation={this.props.navigation}/>*/}
        <BackButtonGroups navigation={this.props.navigation}/>
        
        <Text style={styles.resultsHeading}>Most Popular Results</Text>
        
        <Container>
           
           {/* <Text>List: {JSON.stringify(this.state.restaurantsList)} </Text>  */}
          
          <FlatList
            data={this.state.restaurantsList}
            keyExtractor={(item, index) => index}
            renderItem={({item}) => (
              <View style={styles.listItem}>
                <Image source={{uri: item.photoUrl}} style={styles.placeImage}/>
                <View style={{flex: 3}}>
                  <Text style={styles.placeName}>{item.name}</Text>
                  <Text style={styles.placeAddress}>{item.vicinity}</Text>
                  <Text style={styles.placeVotes}>Votes: {item.votes}</Text>
                </View>
              </View>
            )}
          />
          
        </Container>
        
      </Container>
    );
  };
}

// Get most popular restaurants for current group
async function fetchMostPopular(){
  
  const token = await api.get('token');
  const curGroupId = await api.get("curGroupId");
  const data = { groupCode: curGroupId };
  fetch(SERVER_ADDR + '/getmostpopular', {  
    method: 'POST',  
    headers: {
      'Authorization': 'JWT ' + token,
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },  
    body: JSON.stringify(data),
  })
  .then((response) => {
    // If an error is sent by the server
    if(response.status < 200 || response.status >= 300){ // success codes are 2xx
      resultsViewObj.setState({
        fetchFailed: true,
      });
      return null;
    }
    return response.json();
  })
  .then((responseJson) =>{
    // If server response is successful
    if(responseJson != null){
      resultsViewObj.setState({
        restaurantsList: responseJson.data,
      });
    }
  })
  .then(() => {
    // If server response is successful but zero restaurants are returned
    if(resultsViewObj.state.restaurantsList.length == 0 && !resultsViewObj.state.fetchFailed){
      resultsViewObj.setState({
        noRestaurantsFound: true,
      });
    }
  })
  .catch((error) => {
    // Catch any other errors
    console.error(error);
    resultsViewObj.setState({
      fetchFailed: true,
    });
  });
}


/*
  See:
   - https://facebook.github.io/react-native/docs/flatlist.html
*/
