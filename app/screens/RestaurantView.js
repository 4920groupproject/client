/*
Notes:
  - NativeBase is less robust when handling images;
    if the image is not found it crashes.
  - Should switch to HTTPS later
  - References at end of file
*/

import React, { Component } from 'react';
import { Platform, StyleSheet, Dimensions, Image, Linking } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text,
         View, DeckSwiper, Card, CardItem, Thumbnail,
       } from 'native-base';
import AppHeader from '../components/header/AppHeader';
import BackButtonGroups from '../components/header/BackButtonGroups';
import styles from './styles/RestaurantView.styles';
import SeeResultsForm from '../components/restaurantView/SeeResultsForm';
import { api } from "../config/api";


const SERVER_ADDR = 'https://aqueous-bayou-78971.herokuapp.com/' // 'http://192.168.0.20:8081/'

var restaurantViewObj; // set as global var for use in helper functions (see assignment below)

const DEFAULT_LATITUDE = -33.917347;  // default to UNSW
const DEFAULT_LONGITUDE = 151.231268;

export default class RestaurantViewer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: DEFAULT_LATITUDE,  
      longitude: DEFAULT_LONGITUDE,
      error: null,
      restaurantsList: [],
      fetchFailed: false,
      noRestaurantsFound: false,
      curItem: 0,
      popularRestaurantsList: [],
      popularAvailable: false,
      // popularNoRestaurantsFound: true,
      popularFetchFailed: false,
    }
  }

  async componentDidMount() { 
    restaurantViewObj = this; // set as global var for use in helper functions
    
    if(navigator.geolocation.getCurrentPosition){
      // this sets latitude and longitude, and geoSuccess() calls fetchRestaurants()
      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    } 
    else {
      await fetchMostPopular();
      if (this.state.popularAvailable){
        this.props.navigation.navigate('Results');
      } else {
        fetchRestaurants(); // could not get location; fetch restaurants near default location (but should be handled by geoError())
      }
    }
  }
  
  render() {
    
    {/* The order of these if statements for various errors is important */}
    
    if(this.state.popularAvailable){
      this.props.navigation.navigate('Results');
    }
    
    if(this.state.fetchFailed){
      return (
        <View>
          <BackButtonGroups navigation={this.props.navigation}/>
          <Text style={styles.preloadMessage}>Could not fetch from server.</Text>
        </View>
      );
    }
    
    // if (this.state.restaurantsList.length == 0 
              // && this.state.latitude == DEFAULT_LATITUDE && this.state.longitude == DEFAULT_LONGITUDE){
      // return (
        // <View>
          // <BackButtonGroups navigation={this.props.navigation}/>
          // <View><Text style={styles.preloadMessage}>Getting your GPS location...</Text></View>
        // </View>
      // );
    // }
    
    if(this.state.noRestaurantsFound){
      return (
        <View>
          <BackButtonGroups navigation={this.props.navigation}/>
          <Text style={styles.preloadMessage}>There are no restaurants near your location.</Text>
        </View>
      );
    }
    
    if (this.state.restaurantsList.length == 0){
      return (
        <View>
          <BackButtonGroups navigation={this.props.navigation}/>
          <View><Text style={styles.preloadMessage}>Loading...</Text></View>
        </View>
      );
    }
    
    {/* If no errors: */}
    return (
      <Container /* make the background blue??? */>

        {/*<AppHeader navigation={this.props.navigation}/>*/}
        <BackButtonGroups navigation={this.props.navigation}/>
        
        <View style={{flexDirection: 'column'}}>
        
          {/*
            View for DeckSwiper
          */
          }
          <View style={{}}>
            <DeckSwiper
              looping={false}
              dataSource={this.state.restaurantsList}
              onSwipeRight={() => {
                  // updateRestaurantsList();
                  sendLike();
                }
              }
              onSwipeLeft={() => {
                  // updateRestaurantsList();
                  sendDislike();
                }
              }
              renderEmpty={() =>{
                  setTimeout(() => {}, 2000);
                  this.props.navigation.navigate('RestaurantView') 
                }
              }
              renderItem={item =>
                <Card style={{ elevation: 3 /*elevation means shadow*/}}>
                
                  <CardItem>
                    <Body>
                      <Image source={{uri: item.photoUrl}} style={styles.restaurantImage}/>
                    </Body>
                  </CardItem>
                  
                  <CardItem style={styles.restuarantDetailsCardItem} >
                    <Text style={styles.name}>{item.name}</Text>
                    <Icon name="star" style={styles.starIcon} />
                    <Text style={styles.stars}>{item.rating} </Text>
                  </CardItem>
                  
                  <CardItem style={styles.attributionCardItem}>
                    <Text style={styles.attributionText}>Photo Attribution: </Text>
                    <Text style={[{color: 'blue'}, styles.attributionText]} onPress={() => Linking.openURL(attributionUrl(item.photoAttribution))}>
                      {attributionText(item.photoAttribution)}
                    </Text>
                  </CardItem>
                  
                </Card>
              }
            />
          </View>
          
          {/*
            View containing green and red thumbs and text 'SWIPE'
          */}
          <View style={styles.arrowBar}>
            <Icon name="thumbs-down" style={styles.dislike} />
            <Text style={styles.swipeText}>Swipe!</Text>
            <Icon name="thumbs-up" style={styles.like} />
          </View>
          <View style={styles.googleLogo}>
            <Image source={require('../images/powered_by_google_on_white.png')} />
          </View>
          {/*
            View containing debugging text
          */}
          {/*
          <View style={{ flexGrow: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>latitude: {this.state.latitude}</Text>
            <Text>longitude: {this.state.longitude}</Text>
            <Text>restaurantsList.length: {this.state.restaurantsList.length}</Text>
            {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
          </View>
          */}
          
        </View>
        
      </Container>
    );
  };
}

//For Geolocation
async function geoSuccess(position) {
  restaurantViewObj.setState({
    latitude: position.coords.latitude,
    longitude: position.coords.longitude,
    error: null,
  });

  await fetchMostPopular();
  if(restaurantViewObj.state.popularAvailable){
    restaurantViewObj.props.navigation.navigate('Results');
  }
  else{
    fetchRestaurants();
  }
};

var geoOptions = {
  enableHighAccuracy: false, // 'true' is apparently slower (have not verified)
  timeout: 3000,
  maximumAge: 30000,
};

async function geoError(err) {
  await fetchMostPopular();
  if(restaurantViewObj.state.popularAvailable){
    restaurantViewObj.props.navigation.navigate('Results');
  }
  else{
    fetchRestaurants(); // fetch using default latitude/longitude
  }
};

// Fetch
async function fetchRestaurants(){
  console.warn("REACHED fetchRestaurants");
  const token = await api.get('token');
  const curGroupId = await api.get("curGroupId");
  const data = {
    latitude: restaurantViewObj.state.latitude,
    longitude: restaurantViewObj.state.longitude,
    groupCode: curGroupId,
  };
  console.warn("data = " + JSON.stringify(data));

  fetch(SERVER_ADDR + 'getplaces', {  
    method: 'post',
    headers: {
      'Authorization': 'JWT ' + token,
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },  
    body: JSON.stringify(data),
  })
  .then((response) => {
    // If an error is sent by the server
    if(response.status < 200 || response.status >= 300){ // success codes are 2xx
      restaurantViewObj.setState({
        fetchFailed: true,
      });
      return null;
    }
    return response.json();
  })
  .then((responseJson) =>{
    // If server response is successful
    if(responseJson != null){
      // append new places to current restaurantsList
      var newArray = restaurantViewObj.state.restaurantsList;
      console.warn("responseJson.places = " + responseJson.places);
      newArray = newArray.concat(responseJson.places);
      console.warn("newArray = " + newArray);
      restaurantViewObj.setState({
        restaurantsList: responseJson.places, // newArray,
      });
      // prefetch images
      for(i = restaurantViewObj.state.curItem; i < restaurantViewObj.state.restaurantsList.length; i++){
        Image.prefetch(restaurantViewObj.state.restaurantsList[i].photoUrl);
      }
    }
  })
  .then(() => {
    // If server response is successful but there are zero restaurants nearby
    if(restaurantViewObj.state.restaurantsList.length == 0 && !restaurantViewObj.state.fetchFailed){
      restaurantViewObj.setState({
        noRestaurantsFound: true,
      });
    }
  })
  .catch((error) => {
    // Catch any other errors
    console.error(error);
    restaurantViewObj.setState({
      fetchFailed: true,
    });
  });
}

function updateRestaurantsList(){
  if(restaurantViewObj.state.curItem >= restaurantViewObj.state.restaurantsList.length - 2){
    fetchRestaurants();
  }
}

function sendLike(){
  sendSwipe('like');
}

function sendDislike(){
  sendSwipe('dislike');
}

async function sendSwipe(choice){
  const token = await api.get('token');
  const curGroupId = await api.get("curGroupId");
  console.warn("curGroupId = " + curGroupId);
  const data = {
    itemId: restaurantViewObj.state.restaurantsList[restaurantViewObj.state.curItem].id,
    groupCode: curGroupId,
  }
  // Currently not handling fetch errors
  fetch(SERVER_ADDR + choice, {  
    method: 'POST',  
    headers: {  
      'Authorization': 'JWT ' + token,
      'Accept': 'application/json',
      'Content-type':  'application/json'
    },  
    body: JSON.stringify(data),
  });
  // .then((response) => {
    // return null; // do nothing for now, don't handle errors
  // })
  // .catch((error) => {
    // console.error(error);
  // });
  
  // Increment curItem
  restaurantViewObj.setState({
    curItem: restaurantViewObj.state.curItem + 1,
  });
}

// For parsing Google's html_attributions
// e.g. "\u003ca href=\"https://maps.google.com/maps/contrib/114727320476039103791/photos\"\u003eThe Little Snail Restaurant\u003c/a\u003e" 
function attributionUrl(string){
  try{
    const pattern = /href="([^"]*)"/i;
    const matches = pattern.exec(string);
    return matches[1];
  }
  catch(e){
    return "-";
  }
}

function attributionText(string){
  try{
  const pattern = />([^<]*)<\/a>/i;
  const matches = pattern.exec(string);
  return matches[1];
  }
  catch(e){
    return "-";
  }
}


// Get most popular restaurants for current group
async function fetchMostPopular(){
  
  const token = await api.get('token');
  const curGroupId = await api.get("curGroupId");
  const data = { groupCode: curGroupId };
  fetch(SERVER_ADDR + 'getmostpopular', {  
    method: 'POST',  
    headers: {
      'Authorization': 'JWT ' + token,
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },  
    body: JSON.stringify(data),
  })
  .then((response) => {
    // If an error is sent by the server
    if(response.status < 200 || response.status >= 300){ // success codes are 2xx
      restaurantViewObj.setState({
        popularFetchFailed: true,
      });
      return null;
    }
    return response.json();
  })
  .then((responseJson) =>{
    // If server response is successful
    if(responseJson != null){
      restaurantViewObj.setState({
        popularRestaurantsList: responseJson.data,
      });
      // If popular results available
      if(restaurantViewObj.state.popularRestaurantsList.length > 0){
        restaurantViewObj.setState({
          popularAvailable: true,
        });
      }
    }
  })
  .then(() => {
    // If server response is successful but zero restaurants are returned
    if(restaurantViewObj.state.popularRestaurantsList.length == 0 && !restaurantViewObj.state.popularFetchFailed){
      restaurantViewObj.setState({
        popularAvailable: false,
      });
    }
  })
  .catch((error) => {
    // Catch any other errors
    console.error(error);
    restaurantViewObj.setState({
      popularFetchFailed: true,
    });
  });
}



/*
Code modified from: 
    - https://facebook.github.io/react-native/docs/network.html
    - https://react-native-training.github.io/react-native-elements/
    - http://docs.nativebase.io/Components.html
    - https://facebook.github.io/react-native/docs/geolocation.html
    - https://hackernoon.com/react-native-basics-geolocation-adf3c0d10112
    - https://stackoverflow.com/questions/33553112/react-native-asyncstorage-fetches-data-after-rendering, by Nima Soroush.
*/
