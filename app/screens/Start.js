import React, { Component } from 'react';
import { 
  Dimensions,
  Image,
  View,
  Text,
} from 'react-native';
import styles from './styles/Start.styles';

import RegisterLoginButtons from '../components/login/RegisterLoginButtons';
import { api } from '../config/api';

class Start extends Component {

  componentWillMount() { 
    const token = api.get('token')
    .then( async (response)  => {
      const token =  await response;
      if(token != null) {
        this.props.navigation.navigate('Groups')
      }
    })
  }

  render() {
    return (
      <View behaviour="padding" style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            source={require('../images/2_transparent.png')}
          />       
            <Text style={styles.title}>Keebus helps you decide!</Text>
          <View style={styles.formContainer}>
          </View>
        </View>
        <RegisterLoginButtons navigation={this.props.navigation} />
      </View>
    );
  }
}

export default Start;
