/*
Notes:
  - NativeBase is less robust when handling images;
    if the image is not found it crashes.
  - Should switch to HTTPS later
  - References at end of file
*/

import React, { Component } from 'react';
import { Platform, StyleSheet, Dimensions, Image, FlatList, TouchableOpacity, } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text,
         View, Thumbnail,
       } from 'native-base';

import AppHeader from '../components/header/AppHeader';
import styles from './styles/Groups.styles';
import { api } from '../config/api'

const REQUEST_URL = 'https://aqueous-bayou-78971.herokuapp.com';

var groupsViewObj; // set as global var for use in helper functions (see assignment below)

export default class Groups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      groupsList: [],
      tempGroupsList: [{"id": "1", "name": "Group Sydney", "members": ['Sam', 'Scott']}, {"id": "2", "name": "Group Melbourne", "members": ['Mike', 'Mark', 'Matt']}],
      fetchFailed: false,
      noGroupsFound: false,
      userId: 3456, // IMPLEMENT THIS!!!
      groupId: 7, // IMPLEMENT THIS!!!
    }
  }

  componentDidMount() { 
    groupsViewObj = this; // set as global var for use in helper functions
    fetchGroups(); 
  }
  
  render() {
    
    {/* The order of these if statements for various errors is important */}
    
    // if(this.state.fetchFailed){
      // return (
        // <View><Text style={styles.preloadMessage}>Could not fetch from server.</Text></View>
      // );
    // }
    
    // if(this.state.noGroupsFound){
      // return (
        // <View><Text style={styles.preloadMessage}>You are not part of any groups yet.</Text></View>
      // );
    // }
    
    // if (this.state.groupsList.length == 0){
      // return (
        // <View>
          // <View><Text style={styles.preloadMessage}>Loading...</Text></View>
        // </View>
      // );
    // }
    {/* If no errors: */}
      console.log("this.groupsList => ", this.state.groupsList)
    return (
      <Container style={{backgroundColor: '#3498db'}}>

        <AppHeader navigation={this.props.navigation}/>
        
        <Container>
        
          <TouchableOpacity style={styles.mainButton} 
              onPress={ () => this.props.navigation.navigate('CreateGroup') }
          >
            <Text style={styles.mainButtonText}>Create Group</Text>
          </TouchableOpacity>        
        
          <TouchableOpacity style={styles.mainButton} 
              onPress={ () => this.props.navigation.navigate('JoinGroup') }
          >
            <Text style={styles.mainButtonText}>Join Group</Text>
          </TouchableOpacity>        
        
          <Text style={styles.groupsHeading}>Groups</Text>
          <Text style={styles.groupsSubheading}>All group members must join before swiping</Text>
        
          <FlatList
            data={this.state.groupsList}
            keyExtractor={(item, index) => index}
            renderItem={({item}) => (
              <View style={styles.listItem}>
                {/*// <View>*/}
                  <Text style={styles.groupName}>{item.name}</Text>
                  <Text style={styles.groupMembers}>Members: {item.members.toString()}</Text>
                  <Text style={styles.groupMembers}>Code: {item.id}</Text>

                  <View style={styles.itemButtonContainer}>
                    <TouchableOpacity style={styles.itemButton} 
                      onPress={ () => {
                        api.set("curGroupId", item.id);
                        this.props.navigation.navigate('RestaurantView');
                        }
                      }
                    >
                      <Text style={styles.itemButtonText}>
                        Swipe
                      </Text>
                    </TouchableOpacity>      

                    <TouchableOpacity style={styles.itemButton} 
                      onPress={ () => {
                        api.set("curGroupId", item.id);
                        this.props.navigation.navigate('Results');
                        }
                      }                        
                    >
                      <Text style={styles.itemButtonText}>
                        See Popular
                      </Text>
                    </TouchableOpacity>      
                    
                    <TouchableOpacity style={styles.itemButton} 
                      onPress={ () => {
                        api.set("curGroupId", item.id);
                        this.props.navigation.navigate('Chat');
                        }
                      }                          
                    >
                      <Text style={styles.itemButtonText}>
                        Chat
                      </Text>
                    </TouchableOpacity> 
                    
                  </View>
                  
              </View>
            )}
          />
        </Container>
        
      </Container>
    );
  };
}

// Get groups matching current userId
async function fetchGroups(){
  const token = await api.get('token')
  const newGroupName = await api.get('groupName')
  console.log("newGroupName: ", newGroupName)

  fetch(REQUEST_URL + '/getgroups', {  
    method: 'GET',  
    headers: {  
      'Authorization': 'JWT ' + token,
    },  
  })
  .then((response) => {
    // If an error is sent by the server
    if(response.status < 200 || response.status >= 300){ // success codes are 2xx
      groupsViewObj.setState({
        fetchFailed: true,
      });
      return null;
    }
    return response.json();
  })
  .then((responseJson) =>{
    // If server response is successful
    if(responseJson != null){
      //responseJson[responseJson.length - 1].name = newGroupName; 
      groupsViewObj.setState({
        groupsList: responseJson,
      });
      console.log("responseJson => ", responseJson)
    }
  })
  .then(() => {
    // If server response is successful but zero groups are returned
    if(groupsViewObj.state.groupsList.length == 0 && !groupsViewObj.state.fetchFailed){
      groupsViewObj.setState({
        noGroupsFound: true,
      });
    }
  })
  .catch((error) => {
    // Catch any other errors
    console.error(error);
    groupsViewObj.setState({
      fetchFailed: true,
    });
  });
}


/*
  See:
   - https://facebook.github.io/react-native/docs/flatlist.html
*/
