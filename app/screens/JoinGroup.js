import React, { Component } from 'react';
import { 
  Image,
  View,
  KeyboardAvoidingView,
  ActivityIndicator,
  TouchableOpacity,
  Clipboard,
  TextInput,
} from 'react-native'
import styles from './styles/JoinGroup.styles';

import { Button, Icon, Text } from 'native-base';
import { api } from '../config/api'

/* The structure of this file has been copied from LoginForm.js -Irfan */

/* Not sure what these are -Irfan */
// const options = {
  // title: 'Create a group',
  // storageOptions: {
    // skipBackup: true,
    // path: 'images',
  // },
// }


const REQUEST_URL = 'https://aqueous-bayou-78971.herokuapp.com';

var joinGroupObj; // set as global var for use in helper functions (see assignment below)

class CreateGroup extends Component {
  constructor(props) {
    super(props)
    this.state = {
      groupcode: "0",
      message: "-",
      fetchFailed: false,
    };
  }

  
  componentDidMount() { 
    joinGroupObj = this; // set as global var for use in helper functions
  }
  
  render() {
      
    return(
      <KeyboardAvoidingView behaviour="padding" style={styles.container}>
      {/* There seems to be a bug with ios when using KeyboardAvoidingView */}

        <Button transparent iconLeft light
          onPress={
          () => this.props.navigation.navigate('Groups')
        }>
          <Icon name='arrow-back' />
          <Text>Back</Text>
        </Button>

        
        <TextInput 
          placeholder="Group Code"
          placeholderTextColor="rgba(255,255,255, 0.7)"
          style={styles.input}
          underlineColorAndroid={'transparent'}
          onChangeText={(text) => this.setState({
                                groupcode: text,
                              })}
        />    

        <TouchableOpacity style={styles.buttonContainer}
          onPress={ () => {
              fetchJoinGroup();
              this.setState({
                message: "Loading",
              });
            }
          }
        >
          <Text style={styles.buttonText}>JOIN GROUP</Text>
        </TouchableOpacity>
     
        
        <Text style={styles.statusLabel}>
          Status: {this.state.message}
        </Text>

      </KeyboardAvoidingView>
      
    );
  }
}

// Create a group
async function fetchJoinGroup(){
  const token = await api.get('token');
  const data = { groupCode: joinGroupObj.state.groupcode }
  fetch(REQUEST_URL + '/joingroup', {  
    method: 'POST',  
    headers: {  
      'Authorization': 'JWT ' + token,
      'Accept': 'application/json',
      'Content-type':  'application/json'
    },  
      body: JSON.stringify(data),
  })
  .then((response) => {
    console.log("Join Group response: ", response)
    // If an error is sent by the server
    if(response.status < 200 || response.status >= 300){ // success codes are 2xx
      joinGroupObj.setState({
        fetchFailed: true,
        message: "Could not connect to server",
      });
      return null;
    }
    return response.json();
  })
  .then((responseJson) =>{
    console.log("Join Group ", responseJson)
    // If server response is successful
    if(responseJson != null){
      joinGroupObj.setState({
        message: responseJson.message,
      });
    }
  })
  .catch((error) => {
    // Catch any other errors
    console.error(error);
    joinGroupObj.setState({
      fetchFailed: true,
      message: "Could not connect to server",
    });
  });
}

export default CreateGroup; 
