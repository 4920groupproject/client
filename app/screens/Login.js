import React, { Component } from 'react';
import { 
  Image,
  View,
  KeyboardAvoidingView
} from 'react-native';
import { Button, Icon, Text } from 'native-base';
import styles from './styles/Login.styles';

import LoginForm from '../components/login/LoginForm';


class Login extends Component {
  render() {
    return (
      <KeyboardAvoidingView behaviour="padding" style={styles.container}>
      {/* There seems to be a bug with ios when using KeyboardAvoidingView */}

          <Button transparent iconLeft light
            onPress={ () => this.props.navigation.navigate('Start') }
          >
            <Icon name='arrow-back' />
            <Text>Back</Text>
          </Button>

        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            source={require('../images/2_transparent.png')}
          />       
            <Text style={styles.title}>Keebus helps you decide!</Text>
          <View style={styles.formContainer}>
          </View>
        </View>
        <LoginForm navigation={this.props.navigation}/>
      </KeyboardAvoidingView>
    );
  }
}

export default Login;
