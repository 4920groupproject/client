import React, { Component } from 'react';
import { 
  Image,
  View,
  KeyboardAvoidingView,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { Button, Icon, Text } from 'native-base';
import ImagePicker from 'react-native-image-picker'

import styles from './styles/Register.styles';
import { api } from '../config/api'

var CryptoJS = require('crypto-js');
var DOMParser = require('xmldom').DOMParser;

const options = {
  title: 'Select image of your profile picture',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
}

import RegistrationForm from '../components/login/RegistrationForm';

const blankProfileUrl = 'http://res.cloudinary.com/devwj3eid/image/upload/v1508479797/blank_profile_picture_aajkxd.png'; 

class Register extends Component {

  constructor(props) {
    super(props)

    this.onPickImage = this.onPickImage.bind(this)
    this.onReset = this.onReset.bind(this)
    this.uploadImage = this.uploadImage.bind(this)

    this.state = {
      avatar: {uri:  blankProfileUrl},
      uploadText: 'Upload Profile Pic',
    }
  }

  componentDidMount() { 
    regViewObj = this; // set as global var for use in helper functions
  }


  uploadImage(uri) {
    let timestamp = (Date.now() / 1000 | 0).toString();
    let api_key = '845944431293756'
    let api_secret = 'I-9CAkRYSPX5ozZniRo5btoEJfE'
    let cloud = 'devwj3eid'
    let hash_string = 'timestamp=' + timestamp + api_secret
    let signature = CryptoJS.SHA1(hash_string).toString();
    let upload_url = 'https://api.cloudinary.com/v1_1/' + cloud + '/image/upload'
  
    let xhr = new XMLHttpRequest();
    xhr.open('POST', upload_url);
    xhr.onload = async () => {
      console.log(xhr.response);
      var str = xhr.response.toString();
      const res = str.substring(14,34)
      console.log(str.substring(14,34))
      api.set("avatar", res)
      api.set("uploadStatus", regViewObj.state.uploadText)
      if(xhr.response === null) {
        regViewObj.setState({uploadText: 'There was an error when uploading your image to the server..'})
      }else {
        regViewObj.setState({uploadText: 'Done!'})
      }
    };
    xhr.upload.onprogress = function(e){
      var done = e.position || e.loaded, total = e.totalSize || e.total
      var present = Math.floor(done/total*100)
      console.log(present, " %")
      regViewObj.setState({uploadText: 'Uploading image ' + present + '%'})
    }
    let formdata = new FormData();
    formdata.append('file', {uri: uri, type: 'image/png', name: 'profile.png'});
    formdata.append('timestamp', timestamp);
    formdata.append('api_key', api_key);
    formdata.append('signature', signature);
    xhr.send(formdata);
  }

  onPickImage() {
    ImagePicker.showImagePicker(options, response => {
      if (!response.didCancel && !response.error) {
        const source = { uri: response.uri }
        this.setState({
          avatar: source,
        })
      }
      this.uploadImage(this.state.avatar.uri)
    })
  }

  onReset() {
    this.setState({
      avatar: null,
    })
  }

  render() {
    return(
      <KeyboardAwareScrollView behaviour="padding" style={styles.container}>
      {/* There seems to be a bug with ios when using KeyboardAvoidingView */}

          <Button transparent iconLeft light
            onPress={() => this.props.navigation.navigate('Start')}>
            <Icon name='arrow-back' />
            <Text>Back</Text>
          </Button>

        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            source={require('../images/2_transparent.png')}
          />       

        <TouchableOpacity style={styles.profileContainer}
          onPress={this.onPickImage}
          title="Select your avatar"
          color="#841584"
        >
          <Image
            style={styles.profilePic}
            source={this.state.avatar}
          />
          <Text style={styles.uploadText}>{this.state.uploadText}</Text>
        </TouchableOpacity>

          <View style={styles.formContainer}>
          </View>
        </View>
        <RegistrationForm navigation={this.props.navigation}/>
      </KeyboardAwareScrollView>

    );
  }
}

export default Register; 
