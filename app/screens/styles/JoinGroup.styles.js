import {  StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  preloadMessage: {
    marginTop: '50%',
    textAlign: 'center',
    color: '#999999',
    fontWeight: '700'
  },
  container: {
    flex: 1,
    backgroundColor: '#3498db'
  },
  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'

  },
  logo: {
    width: 100,
    height: 100,
  },
  title: {
    color: '#FFF',
    marginTop: 10,
    textAlign: 'center',
    opacity: 0.9
  },
  statusLabel: {
    color: '#FFF',
    marginTop: 10,
    opacity: 0.9,
    marginLeft:20 ,
  },
  status: {
    color: '#FFF',
    marginTop: 5,
    opacity: 0.9,
    marginLeft: 20,
  },
  input: {
    height: 40,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    marginTop:30,
    marginBottom: 10,
    marginLeft:20,
    marginRight:20,
  },
  buttonContainer: {
    backgroundColor: '#2980b9',
    marginLeft:20,
    marginRight:20,
    marginTop:10,
    paddingVertical: 15,
  },
  buttonText: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontWeight: '700'
  },
});

export default styles;
