import { StyleSheet, Dimensions, } from 'react-native';

var SCREEN_HEIGHT = Dimensions.get('window').height;
var SCREEN_WIDTH  = Dimensions.get('window').width;

const styles = StyleSheet.create({
  restaurantImage: {
    height: SCREEN_HEIGHT*.55,
    width: SCREEN_WIDTH*.85,
    marginRight:'auto',
    marginLeft:'auto',
  },
  name: {
    flex: 8,
  },
  starIcon: {
    flex: 0.7,
    fontSize:20,
    color: '#dddd00', 
    alignSelf: 'flex-end',
  },
  stars: {
    flex: 1,
    fontSize:12,
    alignSelf: 'flex-end',
  },
  distanceIcon: {
    flex: 0.5,
    fontSize:17,
    color: 'grey', 
    alignSelf: 'flex-end',
  },
  distance: {
    flex: 1.8,
    fontSize:12,
    alignSelf: 'flex-end',
  },
  arrowBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    position: 'absolute',
    top: SCREEN_HEIGHT * 0.7,
  },
  swipeText: {
    flex: 6,
    color: 'grey',
    textAlign: 'center',
    paddingTop:15,
  },
  like: {
    flex:1,
    fontSize: 50,
    color:'#55cc55',
    paddingRight:10,
  },
  dislike: {
    flex:1,
    fontSize: 50,
    color:'#ee5555',
    paddingLeft:15,
    paddingTop:10,
  },
  preloadMessage: {
    marginTop: '50%',
    textAlign: 'center',
    color: '#999999',
    fontWeight: '700'
  },
  googleLogo: {
    flexDirection: 'row',
    position: 'absolute',
    top: SCREEN_HEIGHT * 0.8,
    left: '50%',
    marginLeft: -108,
  },
  restuarantDetailsCardItem: {
    flexDirection: 'row',
    height: 30,
  },
  attributionCardItem: {
    height:25,
    marginBottom:5,
  },
  attributionText: {
    fontSize: 10,
  }
});

export default styles;
