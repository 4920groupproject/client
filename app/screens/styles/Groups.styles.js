import { StyleSheet, Dimensions, } from 'react-native';

var SCREEN_HEIGHT = Dimensions.get('window').height;
var SCREEN_WIDTH  = Dimensions.get('window').width;

const styles = StyleSheet.create({
  preloadMessage: {
    marginTop: '50%',
    textAlign: 'center',
    color: '#999999',
    fontWeight: '700'
  },
  mainButton: {
    backgroundColor: '#2980b9',
    paddingVertical: 15,
    marginTop:20,
    marginLeft: 10,
    marginRight: 10,
  },
  mainButtonText: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontWeight: '700',
  },
  groupsHeading: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontWeight: '700',
    marginTop:15,
    marginBottom:0,
  },
  groupsSubheading: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontWeight: '100',
    marginTop:0,
    marginBottom:5,
    fontSize:10,
  },
  listItem: {
    flexDirection: 'column',
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: '#edf0ff',
    padding:15,
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 10,
  },
  groupName: {
    flex:1,
    fontSize: 16,
    fontWeight: '200',
  },
  groupMembers: {
    flex:1,
    fontSize: 14,
  },
  itemButtonContainer: {
    flexDirection: 'row',
  },
  itemButton: {
    flex: 1,
    backgroundColor: '#49a0d9',
    paddingVertical: 5,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 5,
  },
  itemButtonText: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontWeight: '100',
    fontSize: 14,
  }
});

export default styles;
