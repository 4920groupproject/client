import { StyleSheet, Dimensions, } from 'react-native';

var SCREEN_HEIGHT = Dimensions.get('window').height;
var SCREEN_WIDTH  = Dimensions.get('window').width;

const styles = StyleSheet.create({
  preloadMessage: {
    marginTop: '50%',
    textAlign: 'center',
    color: '#999999',
    fontWeight: '700'
  },
  resultsHeading: {
    textAlign: 'center',
    color: '#2980b9',
    fontWeight: '700',
    marginTop:15,
    marginBottom:5,
  },
  listItem: {
    flexDirection: 'row',
    marginTop: 15,
    marginBottom: 15,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor:'#ffffff',
    padding:15,
  },
  placeName: {
    fontSize: 16,
    fontWeight: '200',
  },
  placeAddress: {
    fontSize: 14,
  },
  placeVotes: {
    fontSize: 12,
    color: '#34bc49',
  },
  placeImage: {
    flex: 1,
    height: 75,
    marginRight:5,
  },
});

export default styles;
