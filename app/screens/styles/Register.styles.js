import {  StyleSheet, PixelRatio } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3498db'
  },
  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'

  },
  logo: {
    width: 60,
    height: 60,
  },
  profileContainer: {
     alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
  }, 
  profilePic: {
    width: 100,
    height: 100,
    borderRadius: 30 / PixelRatio.get()
  },
  uploadText: {
    color: '#FFF',
    marginTop: 10,
    width: 160,
    textAlign: 'center',
    opacity: 0.9
  },
});

export default styles;
