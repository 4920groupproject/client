// modified from https://github.com/FaridSafi/react-native-gifted-chat/blob/master/example/App.js
// IMPORTANT: SEE THIS: https://github.com/FaridSafi/react-native-gifted-chat#notes-for-redux
// TO DO: fix timezones (currently 3 hours slow)


import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { Container } from 'native-base';
import {GiftedChat, Actions, Bubble} from 'react-native-gifted-chat';
import CustomActions from './CustomActions'; // TO DO: put this in components
import CustomView from './CustomView'; // TO DO: put this in components
import TimerMixin from 'react-timer-mixin';
import BackButtonGroups from '../components/header/BackButtonGroups';
import { api } from '../config/api'

const REQUEST_URL = 'https://aqueous-bayou-78971.herokuapp.com';
var chatViewObj; // set as global var for use in helper functions (see assignment below)

export default class Chat /*Example*/ extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      loadEarlier: false,
      typingText: null,
      isLoadingEarlier: false,
      curUserId: 1,
      serverPoller: null,
    };

    this._isMounted = false;
    this.onSend = this.onSend.bind(this);
    this.onReceive = this.onReceive.bind(this);
    //this.renderCustomActions = this.renderCustomActions.bind(this);
    this.renderBubble = this.renderBubble.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    // this.onLoadEarlier = this.onLoadEarlier.bind(this);

    // this._isAlright = null;
  }

  componentWillMount() {
    this._isMounted = true;
    chatViewObj = this; // set as global var for use in helper functions
    fetchGetMessages();
    // this.setState(() => {
      // return {
        // messages:  require('./data/messages.js'), // do fetch here or set at start //
      // };
    // });
    
    // this.setTimeout(
      // () => { console.warn('Irfan;s timeout!!!'); },
      // 3000
    // );
  }

  componentDidMount() {
    // Fetch new messages every 5 seconds.
    this.setState({
        serverPoller: setInterval(fetchGetMessages, 5000),
    });
  }

  componentWillUnmount() {
    // NOTE: Unmount is not called in StackNavigator!!!
    
    this._isMounted = false;
    // Remember to clear serverPoller, otherwise it continues as long as the app is open!!!
    clearInterval(this.state.serverPoller);
  }
  
  // onLoadEarlier() {
    // this.setState((previousState) => {
      // return {
        // isLoadingEarlier: true,
      // };
    // });

    // setTimeout(() => {
      // if (this._isMounted === true) {
        // this.setState((previousState) => {
          // return {
            // messages: GiftedChat.prepend(previousState.messages, require('./data/old_messages.js')),
            // loadEarlier: false,
            // isLoadingEarlier: false,
          // };
        // });
      // }
    // }, 1000); // simulating network
  // }

  // answerDemo(messages) {
    // if (messages.length > 0) {
      // if ((messages[0].image || messages[0].location) || !this._isAlright) {
        // this.setState((previousState) => {
          // return {
            // typingText: 'React Native is typing'
          // };
        // });
      // }
    // }

    // setTimeout(() => {
      // if (this._isMounted === true) {
        // if (messages.length > 0) {
          // if (messages[0].image) {
            // this.onReceive('Nice picture!');
          // } else if (messages[0].location) {
            // this.onReceive('My favorite place');
          // } else {
            // if (!this._isAlright) {
              // this._isAlright = true;
              // this.onReceive('Alright');
            // }
          // }
        // }
      // }

      // this.setState((previousState) => {
        // return {
          // typingText: null,
        // };
      // });
    // }, 1000);
  // }

  onSend(messages = []) {
    // Send message text to server
    message = messages[0].text;
    fetchSendMessage(message);

    // Update this message on client side straight away (will need to be refreshed by server later)
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, messages),
      };
    });

  }

  onReceive(text) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, {
          _id: Math.round(Math.random() * 1000000),
          text: text,
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://facebook.github.io/react/img/logo_og.png',
          },
        }),
      };
    });
  }

  // // Not implementing this -Irfan
  // renderCustomActions(props) {
    // if (Platform.OS === 'ios') {
      // return (
        // <CustomActions
          // {...props}
        // />
      // );
    // }
    // const options = {
      // 'Action 1': (props) => {
        // alert('option 1');
      // },
      // 'Action 2': (props) => {
        // alert('option 2');
      // },
      // 'Cancel': () => {},
    // };
    // return (
      // <Actions
        // {...props}
        // options={options}
      // />
    // );
  // }

  renderBubble(props) {
    // this is the other person's bubble -Irfan
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
            backgroundColor: '#f0f0f0',
          }
        }}
      />
    );
  }

  // // Not implementing this -Irfan
  // renderCustomView(props) {
    // return (
      // <CustomView
        // {...props}
      // />
    // );
  // }

  renderFooter(props) {
    // This is the input text area -Irfan
    if (this.state.typingText) {
      return (
        <View style={styles.footerContainer}>
          <Text style={styles.footerText}>
            {this.state.typingText}
          </Text>
        </View>
      );
    }
    return null;
  }

  render() {
    return (
      <Container>
        <BackButtonGroups 
          navigation={this.props.navigation}
          serverPoller={this.state.serverPoller}
        />
        <GiftedChat
          user={{_id: this.state.curUserId}}
          messages={this.state.messages}
          onSend={this.onSend}
          loadEarlier={false} // {this.state.loadEarlier}
          isLoadingEarlier={this.state.isLoadingEarlier}
          renderBubble={this.renderBubble} // This is the other person's bubble -Irfan
          renderFooter={this.renderFooter} // This is the text input
          // onLoadEarlier={this.onLoadEarlier}
          // renderActions={this.renderCustomActions}
          // renderCustomView={this.renderCustomView}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  footerContainer: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
  },
  footerText: {
    fontSize: 14,
    color: '#aaa',
  },
});


// Send a message
async function fetchSendMessage(messageText){
  const token = await api.get('token');
  const curGroupId = await api.get("curGroupId");
  const data = { 
    groupCode: curGroupId,
    message: messageText,
  };
  fetch(REQUEST_URL + '/sendmessage', {  
    method: 'POST',  
    headers: {  
      'Authorization': 'JWT ' + token,
      'Accept': 'application/json',
      'Content-type':  'application/json'
    },  
      body: JSON.stringify(data),
  })
  .then((response) => {
    // If an error is sent by the server
    if(response.status < 200 || response.status >= 300){ // success codes are 2xx
      console.warn("Irfan says: /sendmessage did not return 200 success");
      return null;
    }
    return response.json();
  })
  .then((responseJson) =>{
    // If server response is successful
    if(responseJson != null){
      // do nothing... -Irfan
    }
  })
  .catch((error) => {
    // Catch any other errors
    console.error(error);
    console.warn("Irfan says: sendmessage error" + error);
  });
}

async function fetchGetMessages(){ 
  const token = await api.get('token');
  const curGroupId = await api.get("curGroupId");
  const data = { 
    groupCode: curGroupId,
  };
  fetch(REQUEST_URL + '/getmessages', {  
    method: 'POST',  
    headers: {  
      'Authorization': 'JWT ' + token,
      'Accept': 'application/json',
      'Content-type':  'application/json'
    },  
      body: JSON.stringify(data),
  })
  .then((response) => {
    // If an error is sent by the server
    if(response.status < 200 || response.status >= 300){ // success codes are 2xx
      console.warn("Irfan says: /getmessages did not return 200 success");
      return null;
    }
    return response.json();
  })
  .then((responseJson) =>{
    // If server response is successful
    console.warn(JSON.stringify(responseJson));
    if(responseJson != null){
      chatViewObj.setState({
        curUserId: responseJson.Userid,
        messages: responseJson.messages.reverse(),
      });
    }
  })
  .catch((error) => {
    // Catch any other errors
    console.error(error);
    console.warn("Irfan says: getmessages error" + error);
  });
}
