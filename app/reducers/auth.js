import { AsyncStorage } from 'react-native';

import { LOGIN_SUCCESS, LOGGED_IN, LOGGED_OUT} from "../actions/types";
import { api } from "../config/api";

let initialAuthState = { loggedIn: false };

const authReducer = (state = initialAuthState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      state = Object.assign({}, state, {loggedIn: true});
      api.set('token', action.token); //save the token
      api.set('user', JSON.stringify(action.user));
      return state;
    case LOGGED_OUT:
      state = Object.assign({}, state, {loggedIn: false});
      AsyncStorage.removeItem('token'); //clear token on device
      AsyncStorage.removeItem('user'); //clear token on device
      AsyncStorage.removeItem('avatar'); //clear token on device
      return state;
    default:
      return state;
  }
}
export default authReducer;
