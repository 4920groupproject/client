import { combineReducers } from 'redux';
import authReducer from './auth';
import curGroupReducer from './curGroup';

export default combineReducers({
  authReducer, // add other reducers here
  curGroupReducer,
});
