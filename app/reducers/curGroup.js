import { AsyncStorage } from 'react-native';

import { CREATED_GROUP } from "../actions/types";
import { api } from "../config/api";

let initialCurGroupState = { named : false }
const curGroupReducer = (state = initialCurGroupState, action) => {
  switch (action.type) {
     case CREATED_GROUP:
       state = Object.assign({}, state, { named: true});
       api.set('groupName', action.groupName); //save the token
       return state;
     default:
       return state;
  }
}
 export default curGroupReducer;
