import { AsyncStorage, Alert } from 'react-native';

import { LOGIN_SUCCESS, LOGGED_IN, LOGGED_OUT } from "../../actions/types"
import store from '../../store'

const REQUEST_URL = 'https://aqueous-bayou-78971.herokuapp.com';

var _responseData = '';

async function request(url, data) {
  const response = await fetch(url , {
    method: "POST", 
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data),
  })
  const jsonResponse = response.json()
  .then((responseData) => {
    _responseData = responseData 
  })
  .catch(function(error) {
    console.log('There has been a problem with your fetch operation: ' + error.message);
    throw error;
  });
}

export const api = {

  // TO DO CHECKS FOR LOGIN (e.g. user puts in a value for username/ pass etc)
  login: async (user, password, nav) => { 
      const url = REQUEST_URL + '/auth';
      const data = {
        username: user,
        password: password,
      }
      await request(url, data)
      // if invalid creds return error
      if(_responseData.status_code === 401) {
        Alert.alert(
          _responseData.description,
          'Please check email and password!',
          [ {text: 'OK', onPress: () => {/* add a function on OK */}},],
          { cancelable: false }
        )
      }
      // if successful login navigate to groups page and store jwt token
      if("access_token" in _responseData) {
        store.dispatch({type: LOGIN_SUCCESS, token: _responseData.access_token, user: data.username})
        .then(nav.navigate('Groups'))
      }
  },

  register: async (username, email, password, firstname, lastname, nav) => {
    const avatar = await api.get("avatar") 
    const uploadStatus = await api.get("uploadStatus") 
    console.log("AVATARR IS: ", avatar)
    console.log("uploadSatus IS: ", uploadStatus)
    const url = REQUEST_URL + '/signup'
    const data = {
        username: username,
        email: email,
        password: password,
        firstname: firstname, 
        lastname: lastname,
        avatar: avatar,
      }
      await request(url, data)
      if(_responseData.status === 'success'){
        api.set("user", email)
        api.login(data.email, data.password)
        .then(nav.navigate('Groups'))
        .catch((error)=>{
          throw error
        })
      } else {
        Alert.alert(
          "Registration Failed",
          _responseData.message,
          [ {text: 'OK', onPress: () => {/* add a function on OK */}},],
          { cancelable: false }
        )
      }
  },
  set: (key, value) => {
    value = JSON.stringify(value)
    if (value) return AsyncStorage.setItem(key, value)
    else console.log('not set, stringify failed:', key, value)
  },
  get: (key) => {
    function parseJson (item) {
      try { return JSON.parse(item) }
      catch (e) { return item }
    }
    return AsyncStorage.getItem(key).then(item => parseJson(item)  )
  }
}

