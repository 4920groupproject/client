import React from 'react';
import { addNavigationHelpers, StackNavigator, DrawerNavigator } from 'react-navigation';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Start from "../screens/Start";
import Login from "../screens/Login";
import Register from "../screens/Register";
import RestaurantView from "../screens/RestaurantView";
import Results from "../screens/Results";
import Groups from "../screens/Groups";
import CreateGroup from "../screens/CreateGroup";
import JoinGroup from "../screens/JoinGroup";
import Chat from "../screens/Chat";
import DrawerMenu from "../containers/drawer/DrawerMenu";

export const Drawer = DrawerNavigator(
  {
    Groups: {
      screen: Groups,
      navigationOptions: {
        Drawer: {
          label: 'Groups',
        },
      }
    },
    RestaurantView: {
      screen: RestaurantView,
      navigationOptions: {
        Drawer: {
          label: 'Pick Restaurants',
        },
      }
    },
    Results: {
      screen: Results,
      navigationOptions: {
        Drawer: {
          label: 'Logout',
        },
      }
    }
  }, 
  {
      contentComponent: DrawerMenu,
      drawerWidth: 250
  }
)


export default AppNavigator = StackNavigator({
  Start: {  
    screen: Start,
    navigationOptions: { 
      title: 'Start',
      header: null
    },
 },
  Login: {
    screen: Login,
    navigationOptions: {
      title: 'Login',
      header: null
    }
  },
  Register: {
    screen: Register,
    navigationOptions: {
      title: 'Register',
      header: null
    }    
  },
  RestaurantView: {
    screen: RestaurantView,
    navigationOptions: {
      title: 'RestaurantView',
      header: null
    }
  },
  Results: {
    screen: Results,
    navigationOptions: {
      title: 'Results',
      header: null
    }
  },
  Groups: {
    screen: Drawer,
    navigationOptions: {
      title: 'Groups',
      header: null
    }
  },
  CreateGroup: {
    screen: CreateGroup,
    navigationOptions: {
      title: 'CreateGroup',
      header: null
    }
  },
  JoinGroup: {
    screen: JoinGroup,
    navigationOptions: {
      title: 'JoinGroup',
      header: null
    }
  },
  Chat: {
    screen: Chat,
    navigationOptions: {
      title: 'Chat',
      header: null
    }
  },
});

const AppWithNavigationState = ({ dispatch, nav }) => (
  
  <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
);


AppWithNavigationState.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  nav: state.nav,
});

connect(mapStateToProps)(AppWithNavigationState);
