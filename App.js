import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';

import store from './app/store';
import AppWithNavigationState from './app/config/routes';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppWithNavigationState />
      </Provider>
    );
  }
}


export default keebus = () => <AppWithNavigationState />;
console.disableYellowBox = true;
AppRegistry.registerComponent('Keebus', () => App);
